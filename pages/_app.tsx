import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { IntlProvider } from 'react-intl'
import { useRouter } from 'next/router';
import { useMemo } from 'react';
import JP from "../compiled-locales/jp.json";
import EN from "../compiled-locales/en.json";

export default function App({ Component, pageProps }: AppProps) {
  const { locale } = useRouter();

  const messages = useMemo(() => {
    switch (locale) {
      case "ja-JP":
        return JP;
      default:
        return EN;
    }
  }, [locale]);

  return (
    <IntlProvider
      locale={locale ? locale : "en-US"}
      messages={messages}
      onError={() => null}>
      <Component {...pageProps} />
    </IntlProvider>)
}
