import Link from "next/link"

export default function Test() {
    const name = ""
    const count = 1

    return (
        <div>
            <div>Just simple content</div>
            <div>
                Hello <strong title="this is your name">{name}</strong>, you have {count} unread message(s). <Link href="/msgs">Go to messages</Link>.
            </div>
        </div>
    )
}