import Link from "next/link"
import { useRouter } from "next/router";
import { FormattedMessage } from "react-intl";

export default function Msg() {
    const name = "name in Msg"
    const count = 1

    const router = useRouter()
    const { locale, locales, defaultLocale } = router

    return (
        <div>
            <p>Current locale: {locale}</p>
            <p>Default locale: {defaultLocale}</p>
            <p>Configured locales: {JSON.stringify(locales)}</p>
            <div>Just simple content</div>
            <div>
                Hello <strong title="this is your name">{name}</strong>, you have {count} unread message(s). .
            </div>
            <div>  <FormattedMessage
                defaultMessage="simpleTitleTest"
                description="トップページで利用されているタイトルリソースです" />
            </div>
            Hello <strong ><FormattedMessage
                defaultMessage="nameTitle"
                description="トップページで利用されているタイトルリソースです" /></strong>, you have {count} unread message(s). <Link href="/test">Go to messages</Link>.
        </div>
    )
}